const Glue = require('@hapi/glue');

const manifest = require('./lib/manifest');

const options = {
  relativeTo: __dirname
};

if (process.env.NODE_ENV !== 'production') {
  manifest.register.plugins.push({
    plugin: 'blipp',
    options: {
      showAuth: true
    }
  });
}

// Show bad promises
process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at:', p, 'reason:', reason);
});

const startServer = async () => {
  const server = await Glue.compose(manifest, options);
  await server.start();
};

module.exports = startServer;
