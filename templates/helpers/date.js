const ms = require('ms');

module.exports = function(timestamp = 0) {
  return ms(Number(timestamp), { long: true });
};
