exports.home = {
  handler: (request, h) => {
    const data = {
      title: 'Home Page',
      user: {
        firstname: 'John',
        lastname: 'Doe',
        age: 30
      }
    };
    return h.view('index', data);
  }
};

exports.about = {
  handler: (request, h) => {
    return h.view('about', {
      title: 'About Us',
      terms: 'Fantastic terms... blah blah blah'
    });
  }
};

exports.nes = {
  handler: (request, h) => {
    return h.view('nes', {
      title: 'Websocket'
    });
  }
};
