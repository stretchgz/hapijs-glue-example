const path = require('path');
const fs = require('fs');
const Boom = require('@hapi/boom');
const cryptiles = require('@hapi/cryptiles');
const imageType = require('image-type');
const makeDir = require('make-dir');
const pSettle = require('p-settle');

exports.uploadImages = {
  payload: {
    output: 'stream',
    multipart: true,
    parse: true,
    maxBytes: 1048576, // 1MB
    timeout: 5 * 1000 // 5s
  },
  description: 'Upload files',
  tags: ['Image'],
  handler: async (request, h) => {
    const CURRENT_YEAR = new Date().getFullYear();
    const CURRENT_MONTH = new Date().getMonth() + 1;
    const UPLOAD_PATH = `public/uploads/${CURRENT_YEAR}/${CURRENT_MONTH}`;
    const NEW_PATH = path.join(__dirname, '..', UPLOAD_PATH);
    try {
      await makeDir(NEW_PATH); // create directories `/uploadDirectory/year/month`
      const result = await uploader(request.payload.file, {
        path: NEW_PATH,
        year: CURRENT_YEAR,
        month: CURRENT_MONTH
      });
      return h.view('upload', {
        title: 'Upload',
        success: true,
        raw: result
      });
    } catch (err) {
      const errorMsg = err.isBoom ? err.message : 'Something went wrong';
      return h
        .view('upload', {
          title: 'Upload Error',
          success: false,
          error: true,
          errorMsg
        })
        .code(err.isBoom ? err.output.statusCode : 400);
    }
  }
};

function uploader(input, options) {
  return Array.isArray(input)
    ? _uploadMultipleFiles(input, options)
    : _uploadSingleFile(input, options);
}
function _uploadMultipleFiles(inputs, opt) {
  const writeFiles = inputs.map(x => _uploadSingleFile(x, opt));
  return pSettle(writeFiles);
}
function _uploadSingleFile(input, opt) {
  // only accept image (non-svg)
  const pic = imageType(input._data);
  if (!pic) {
    return Promise.reject(Boom.unsupportedMediaType('not an image'));
  }
  // eject tif
  if (pic.ext === 'tif') {
    return Promise.reject(
      Boom.unsupportedMediaType('image/tiff is not supported')
    );
  }

  const randomFileName = cryptiles.randomString(12);
  const newFileName = `${randomFileName}.${pic.ext}`;
  const filePath = path.format({
    dir: opt.path,
    base: newFileName
  });
  const outputFile = fs.createWriteStream(filePath);

  return new Promise((resolve, reject) => {
    input.on('error', err => reject(err));

    input.pipe(outputFile);

    input.on('end', () => {
      const metaData = {
        publicPath: `/assets/uploads/${opt.year}/${opt.month}/${newFileName}`,
        internal: { filePath, metadata: pic }
      };
      resolve(metaData);
    });
  });
}
