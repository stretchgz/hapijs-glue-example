exports.Login = {
  auth: {
    strategy: 'session',
    mode: 'try'
  },
  tags: ['bell', 'auth'],
  handler: async (request, h) => {
    const isAuthenticated = request.auth.isAuthenticated;
    return h.view('login', {
      isAuthenticated,
      user: request.auth.credentials
    });
  },
  plugins: {
    '@hapi/cookie': { redirectTo: false }
  }
};
exports.BellFacebook = {
  auth: 'facebook',
  tags: ['bell', 'auth'],
  handler: async (request, h) => {
    // findOrInsert user
    const account = request.auth.credentials.profile;
    // set session id
    const sid = `facebook:${account.id}`;
    // set session
    try {
      await request.server.app.cache.set(
        sid,
        {
          account
        },
        0
      );
      request.cookieAuth.set({ sid: sid });
      return h.redirect('/bell');
    } catch (error) {
      request.cookieAuth.set({ sid: sid });
      return h.redirect('/bell?fail=true');
    }
  }
};
exports.Logout = {
  auth: 'session',
  tags: ['bell', 'auth'],
  handler: async (request, h) => {
    // read cookie
    const sid = request.state.sid.sid;
    // drop session in catbox
    request.server.app.cache.drop(sid);
    // clear session cookie
    request.cookieAuth.clear();
    return h.redirect('/bell?logged_out=true');
  }
};
