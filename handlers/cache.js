const Joi = require('@hapi/joi');

exports.cache = {
  validate: {
    params: Joi.object({
      id: Joi.string().allow('')
    }),
    query: false
  },
  tags: ['cache', 'catbox-redis'],
  description: 'Route cached with catbox-redis',
  handler: async (request, h) => {
    const id = request.params.id ? encodeURIComponent(request.params.id) : 9000;
    const {
      result,
      cached,
      report
    } = await request.server.methods.User.getById(id); // generate fake user data

    return h.view('cache', {
      title: 'Server Cache',
      user: result,
      cached,
      report
    });
  }
};
