module.exports = {
  extends: ["prettier"],
  plugins: ["prettier"],
  parserOptions: {
    ecmaVersion: 2018
  },
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    node: true
  },
  rules: {
    "prettier/prettier": [
      "error",
      {
        singleQuote: true,
        printWidth: 80,
        tabWidth: 2,
        semi: true,
        useTabs: false,
        trailingComma: "none"
      }
    ]
  }
};
