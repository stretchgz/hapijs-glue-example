const faker = require('faker');
const Joi = require('@hapi/joi');

const register = async (server, options) => {
  // register nes
  await server.register({
    plugin: require('@hapi/nes'),
    options: {
      onMessage
    }
  });

  async function onMessage(socket, message) {
    console.log(message);
    const echo = {
      type: 'broadcast',
      sender: socket.id,
      data: message
    };
    return server.broadcast(echo);
  }

  // nes
  server.subscription('/item/{id}', {
    onSubscribe: async (socket, path, params) => {
      console.log('[NES] Server.onSubscribe');
      console.log(
        `path: ${JSON.stringify(path)}, params: ${JSON.stringify(params)}`
      );
    },
    onUnsubscribe: async (socket, path, params) => {
      console.log('[NES] Server.onUnsubscribe');
      console.log('path: ', JSON.stringify(path));
      console.log('params: ', JSON.stringify(params));
    }
  });
  server.subscription('/time');

  server.route([
    {
      method: 'GET',
      path: '/api/ping',
      options: {
        tags: ['api'],
        description: 'Server health check',
        handler: (request, h) => {
          return {
            status: 'ok'
          };
        },
        response: {
          schema: Joi.object({
            status: Joi.string()
              .required()
              .valid('ok')
          })
        }
      }
    },
    {
      method: 'GET',
      path: '/api/user/{id}',
      options: {
        validate: {
          params: Joi.object({
            id: Joi.string()
              .alphanum()
              .max(50)
              .required()
          })
        },
        tags: ['api', 'fake'],
        description: 'Generate fake user profile',
        handler: (request, h) => {
          return {
            userId: String(request.params.id),
            displayName: faker.name.findName(),
            email: faker.internet.email(),
            avatar: faker.internet.avatar()
          };
        },
        response: {
          schema: Joi.object({
            userId: Joi.string()
              .required()
              .example('some-user-id'),
            displayName: Joi.string()
              .required()
              .example('cool-username'),
            email: Joi.string()
              .email()
              .required()
              .example('user@example.com'),
            avatar: Joi.string()
              .uri()
              .required()
              .example('https://example.com/user/user-avatar.jpg')
          })
        }
      }
    },
    {
      method: 'GET',
      path: '/realtime',
      options: {
        id: 'hello',
        handler: (request, h) => {
          request.socket.publish('/time', {
            id: 90,
            message: new Date().getTime()
          });
          request.socket.publish('/item/5', {
            id: 555,
            message: 'this is item #5'
          });
          request.server.broadcast('welcome!');

          return {
            id: 'hello',
            path: request.path
          };
        }
      }
    }
  ]);
};

exports.plugin = {
  register,
  name: 'prefixed API routes',
  version: '1.0.0',
  once: true
};
