const Path = require('path');
const Config = require('getconfig');
const ms = require('ms');
const faker = require('faker');
const isProduction = process.env.NODE_ENV === 'production';

const delay = timeout => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};

const register = (server, options) => {
  server.views({
    defaultExtension: 'hbs',
    isCached: isProduction,
    engines: {
      hbs: require('handlebars')
    },
    relativeTo: Path.resolve(__dirname, '..'),
    layout: 'default',
    path: './templates',
    helpersPath: './templates/helpers',
    layoutPath: './templates/layout',
    partialsPath: './templates/partials'
  });

  // render error page
  server.ext('onPreResponse', (request, h) => {
    if (!request.response.isBoom) {
      return h.continue;
    }
    console.log(request.response);
    return h
      .view('better404', request.response)
      .code(request.response.output.statusCode);
  });

  /**
   * Catbox configuration
   */
  const cache = server.cache({
    cache: 'redisCache', // catbox cache name, see server.cache in `lib/manifest.js`
    segment: 'sessions',
    expiresIn: 7 * 24 * 60 * 60 * 1000 // 7 days
  });
  server.app.cache = cache;

  /**
   * Authentication Strategy
   */
  server.auth.strategy('session', 'cookie', {
    cookie: {
      password: Config.cookie.password,
      isSecure: isProduction
    },
    redirectTo: '/bell/facebook',
    validateFunc: async function(request, session) {
      const cached = await cache.get(session.sid);
      const out = {
        valid: !!cached
      };

      if (out.valid) {
        out.credentials = cached.account;
      }

      return out;
    }
  });
  /**
   * Visit https://developers.facebook.com/apps/ and create a new app.
   * Add Product, choose Facebook Login,
   * Valid OAuth redirect URIs: http://localhost:9000/bell/facebook
   */
  server.auth.strategy('facebook', 'bell', {
    provider: 'facebook',
    isSecure: isProduction,
    password: Config.facebook.password,
    clientId: Config.facebook.clientId, // App ID
    clientSecret: Config.facebook.clientSecret // App Secret
  });
  /**
   * Server Methods
   */
  server.method(
    'User.getById',
    async uid => {
      // Simulate a long operation
      await delay(1500);

      return {
        uid,
        email: faker.internet.email(),
        displayName: faker.name.findName()
      };
    },
    {
      cache: {
        cache: 'redisCache',
        expiresIn: ms('60s'),
        generateTimeout: ms('2s'),
        staleIn: ms('50s'),
        staleTimeout: ms('2s'),
        getDecoratedValue: true
      }
    }
  );
  /**
   * Admin routes
   */
  server.route([
    {
      method: 'GET',
      path: '/',
      options: require('../handlers/home').home
    },
    {
      method: 'GET',
      path: '/about',
      options: require('../handlers/home').about
    },
    {
      method: 'GET',
      path: '/nes',
      options: require('../handlers/home').nes
    },
    /**
     * Bell Authentications
     */
    {
      method: 'GET',
      path: '/bell',
      options: require('../handlers/bell').Login
    },
    {
      method: 'GET',
      path: '/bell/logout',
      options: require('../handlers/bell').Logout
    },
    {
      method: 'GET',
      path: '/bell/facebook',
      options: require('../handlers/bell').BellFacebook
    },
    /**
     * Upload Images
     */
    {
      method: 'GET',
      path: '/upload',
      options: {
        tags: ['Image'],
        handler: (request, h) => {
          return h.view('upload', { title: 'Upload Images' });
        }
      }
    },
    {
      method: 'POST',
      path: '/upload',
      options: require('../handlers/upload').uploadImages
    },
    /**
     * Server cached routes via cat-box
     */
    {
      method: 'GET',
      path: '/cache/{id?}',
      options: require('../handlers/cache').cache
    },
    /**
     * Server static files
     */
    {
      method: 'GET',
      path: '/assets/{p*}',
      options: {
        description: 'Serve static files',
        handler: {
          directory: {
            path: Path.join(__dirname, '../public'),
            listing: true
          }
        }
      }
    },
    /**
     * Catch all 404 pages
     */
    {
      method: 'GET',
      path: '/{p*}',
      options: {
        description: 'Catch all route',
        handler: (request, h) => {
          return h.view('404', null, { layout: false }).code(404);
        }
      }
    }
  ]);
};

exports.plugin = {
  register,
  name: 'routes for admin connection',
  version: '1.0.0'
};
