const Joi = require('@hapi/joi');

const register = (server, options) => {
  server.route([
    {
      method: 'GET',
      path: '/cool/{uid}',
      config: {
        validate: {
          params: Joi.object({
            uid: Joi.string().guid()
          })
        },
        handler: (request, h) => {
          return h.view('validate', {
            data: request.params.uid
          });
        }
      }
    },
    {
      method: 'GET',
      path: '/output',
      config: {
        handler: (request, h) => {
          return {
            status: 200,
            message: 'output is validated',
            more: 'moar!'
          };
        },
        response: {
          failAction: 'error', // "error" default value, will return 500 instead
          schema: {
            status: Joi.number().valid([200]),
            message: Joi.string().max(100)
          }
        }
      }
    }
  ]);
};

exports.plugin = {
  register,
  name: 'validate route',
  version: '1.0.0'
};
