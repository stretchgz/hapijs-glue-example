const config = require('getconfig');

module.exports = {
  server: {
    host: config.host,
    port: config.port,
    cache: [
      {
        provider: {
          constructor: require('@hapi/catbox-redis'),
          options: {
            partition: 'cache',
            host: config.redis.host,
            port: config.redis.port,
            password: config.redis.password,
            database: config.redis.database
          }
        },
        name: 'redisCache'
      }
    ]
  },
  register: {
    plugins: [
      { plugin: '@hapi/inert' },
      { plugin: '@hapi/vision' },
      { plugin: '@hapi/cookie' },
      { plugin: '@hapi/bell' },
      {
        plugin: 'hapi-swagger',
        options: {
          info: {
            title: 'API Documentation',
            version: '1'
          }
        }
      },
      {
        plugin: '@hapi/good',
        options: {
          reporters: {
            myConsoleReport: [
              {
                module: '@hapi/good-squeeze',
                name: 'Squeeze',
                args: [{ log: '*', response: '*', ops: '*' }]
              },
              { module: '@hapi/good-console' },
              'stdout'
            ]
          }
        }
      },
      {
        plugin: './routes/admin'
      },
      {
        plugin: './routes/prefixed-routes',
        routes: {
          prefix: '/v2'
        }
      }
    ]
  }
};
