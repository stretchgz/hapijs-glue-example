var Nes = require('@hapi/nes/lib/client');

var nesClient = new Nes.Client('ws://localhost:9000');
var jsDom = document.querySelector('#js');
var sendMsg = document.querySelector('#nMessage');
var timespan = document.querySelector('#time');
var item5 = document.querySelector('#item5');
var printResult = document.querySelector('#printResult');
var getSubs = document.querySelector('#getSubscriptions');
var getClientId = document.querySelector('#getClientId');
var unsubscribeBtn = document.querySelector('#unsub');
var nRequest = document.querySelector('#nRequest');
var path = document.querySelector('#path');
var requestPingBtn = document.querySelector('#requestPing');

if (jsDom) {
  nesClient
    .connect()
    .then(() => {
      handleItemSubscribe('/item/5');

      nesClient.subscribe(
        '/time',
        (update, flags) => {
          timespan.innerHTML = new Date(update.message);
          console.log(
            'client subscribe "/time", data: %s, flags: %s',
            JSON.stringify(update),
            JSON.stringify(flags)
          );
        },
        err => {
          if (err) {
            console.error(JSON.stringify(err));
          }
        }
      );
    })
    .catch(err => {
      console.error(err);
    });

  nesClient.onConnect = () => {
    debugPrint('Nes.onConnection: connection established');
    sendMsg.addEventListener('click', sendMsgFuntion, false);
    getSubs.addEventListener('click', getSubsFunction, false);
    getClientId.addEventListener('click', getClientIdFunc, false);
    unsubscribeBtn.addEventListener('click', unSubscribe, false);
    nRequest.addEventListener('click', requestFunc, false);
    requestPingBtn.addEventListener('click', handlePingReqBtn, false);
  };

  nesClient.onError = err => {
    console.log('Nes.onError: %s', JSON.stringify(err));

    sendMsg.removeEventListener('click', sendMsgFuntion, false);
    getSubs.removeEventListener('click', getSubsFunction, false);
    getClientId.removeEventListener('click', getClientIdFunc, false);
    unsubscribeBtn.removeEventListener('click', unSubscribe, false);
    nRequest.removeEventListener('click', requestFunc, false);
    requestPingBtn.removeEventListener('click', handlePingReqBtn, false);
  };

  nesClient.onUpdate = update => {
    debugPrint(
      `Server -> Client
client.onUpdate:
${JSON.stringify(update, null, 2)}`,
      true
    );
  };
}
// send a message to server
function sendMsgFuntion() {
  var data = {
    id: 'hello',
    msg: path.value
  };

  return nesClient.message(data);
}

// list current subscriptions
function getSubsFunction() {
  console.table(nesClient.subscriptions());
  debugPrint(JSON.stringify(nesClient.subscriptions()));
}
// get client's socket id
function getClientIdFunc() {
  debugPrint(`${nesClient.id}`);
}
// unsubscribe a channel
function unSubscribe() {
  nesClient
    .unsubscribe('/item/5', null)
    .then(() => {
      console.log('[nes client]: successfully unsubscribed `/item/5`');
    })
    .catch(err => {
      console.log(err);
    });
}
// ajax, but using websocket
function requestFunc() {
  handleClientRequest({
    path: path.value,
    method: 'GET' // POST, PUT, DELETE
  });
}
function requestHandler(err, payload, statusCode, headers) {
  if (err) {
    console.log('client.request error');
    console.log(JSON.stringify(err));
    return;
  }
  console.log(payload, statusCode, headers);
  if (typeof payload === 'object') {
    printResult.textContent = JSON.stringify(payload, null, 2);
    return;
  }
  printResult.textContent = payload;
}

function debugPrint(message, logConsole = false) {
  if (logConsole) {
    console.log(message);
  }
  printResult.textContent = message;
}

function handleItemSubscribe(path) {
  nesClient.subscribe(path, itemSubscriptionHandler);
}

function itemSubscriptionHandler(message, flags) {
  item5.innerHTML = JSON.stringify(message, null, 4);
  console.log(
    'client subscribe "/item/5", data: %s, flags: %s',
    JSON.stringify(message),
    JSON.stringify(flags)
  );
}

function handlePingReqBtn() {
  if (!requestPingBtn) return;
  const path = requestPingBtn.getAttribute('data-path');
  handleClientRequest(path);
}
function handleClientRequest(path) {
  nesClient
    .request(path)
    .then(handleSuccessfulRequest)
    .catch(err => {
      console.error('client.request error ', err);
      console.error('error type: ', err.type);
    });
}

function handleSuccessfulRequest(result) {
  console.log('client.request success ');
  debugPrint(JSON.stringify(result, null, 2), true);
}
