# README

Using glue to compose a hapi server.

**Note**: master branch has updated to support Hapi v19. For Hapi 16, see branch `v16`.

# Installation

Prerequisites: Node >= 12.x.x

```bash
npm install
```

## Config File

We are using `getconfig` module. All configurations are stored inside `config/` directory. Create `config/local.json` file:

```
{
  "redis": {
    "host": "127.0.0.1",
    "port": 6379,
    "password": "",
    "database": 1
  },
  "cookie": {
    "password": "32-characters-long"
  },
  "facebook": {
    "password": "32-characters-long",
    "clientId": "",
    "clientSecret": ""
  }
}
```

# Start server

```bash
npm start
```

# License

- MIT
