const startServer = require('./server');

startServer()
  .then(() => {})
  .catch(error => {
    console.log('Something went wrong');
    console.log(error);
  });
